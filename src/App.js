import React, {Suspense,useEffect } from 'react';

import Loader from './commonComponents/Loader';
import {
	Routes,
	Route,
	useNavigate,
} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getAuth, signInWithRedirect, GoogleAuthProvider, onAuthStateChanged } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { firebaseConfig } from "./app/firebaseConfig";
import { saveUser } from './features/user/userSlice';

const LoginPage = React.lazy(() => import('./routes/LoginPage.jsx'));
const DashboardPage = React.lazy(() => import('./routes/DashboardPage.jsx'));

function App() {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const auth = getAuth();
	useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                console.log("Firebase auth user :: ", user)
                const uid = user.uid;
                console.log('User logged in.. saving user state and navigating to dashboard...')
                // dispatch( saveUser(user));
          	      localStorage.setItem('google_user_profile', JSON.stringify(user));
                navigate('/dashboard');
            } else {
                // User is signed out
                // ...
                console.log('User Logged out......')
                navigate('/')
            }
        });
    }, [auth]);
	return (
		<>
			<Suspense fallback={ <Loader/> }>
				<Routes>
					<Route exact path="/" element={<LoginPage />} />
					<Route exact path="/dashboard" element={<DashboardPage />} />
				</Routes>
			</Suspense>
		</>
	);
}

export default App;
