import React, { useEffect, useState } from "react";
import Box from "@mui/material/Button";
import { makeStyles } from "@mui/styles";
import "../../App.css";
import TotalAvatars from "../../commonComponents/avtarGroup/AvtarGroup";

const useStyles = makeStyles({
  button: {
    width: "200px",
    minWidth: "200px",
    minHeight: "250px",
    // height: "fit-content",
    display: "block",
    padding: "15px 25px",
    fontSize: "24px",
    textAlign: "center",
    textDecoration: " none",
    outline: " none",
    marginTop: "15px",
    borderRadius: "15px",
    border: "2px #c1c1c1 solid",
    justifyContent: "center",
    cursor: "pointer",
  },
  mainConatiner:{
    display:'flex',
    flexDirection:"column", 
    width:'100%'
  },
  container: {
    display: "flex",
    flexDirection: "column",
    marginBottom: "10px",
    padding: "auto",
    borderRadius: "50%",
    alignItems: "center",
    },
  icon: {
    width: "100px",
    textAlign: "center",
    cursor: "pointer",
  },
  heading: {
    color: "brown",
    fontSize: "20px",
    cursor: "unset",
  },
  TotalAvatars:{
    position:'absolute',
    bottom: '12px',
    
  },
  groupcard:{
    zIndex:1
  },
  mainContainerOut:{
    position:'relative',
    display: 'flex',
    alignItems: 'left',
    justifyContent: 'center',
    marginLeft: '20px',
    flexDirection: 'column',
  }

});
const CustomButton = ({ buttonName, image, onClick, user, givenName ,selection}) => {
  const classes = useStyles();

  return (
    <div className={classes.mainContainerOut}>
    <Box className={classes.button} onClick={onClick} name={buttonName} disabled={true ? selection===true : false } >
      <div className={classes.mainConatiner} onClick={onClick} name={buttonName}  >
      <div className={classes.container} onClick={onClick} name={buttonName} >
        <img
          className={classes.icon}
          src={image}
          alt="img"
          name={buttonName}
        />
      </div>
      <Box sx={{ '&:hover': {backgroundColor:'transparent'}}} 
         className={classes.heading} onClick={onClick} name={buttonName}>
        {buttonName}
      </Box>
      </div>
    </Box>
    <div className={classes.TotalAvatars}>
      <TotalAvatars  buttonName={buttonName} user={user}   />
    </div>
    </div>
  );
};

export default CustomButton;
