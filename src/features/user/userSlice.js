import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    user : '',
}


export const userSlice = createSlice({
    name: 'name',
    initialState,
    reducers: {
        saveUser: (state, action) => {
            console.log("User reducer... saveUser => ", action.payload);
        }
    }
});

export const { saveUser } = userSlice.actions;
export default userSlice.reducer;