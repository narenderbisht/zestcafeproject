import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import CustomButton from "../commonComponents/button/Button";
import { makeStyles } from "@mui/styles";
import tea from "../assets/images/tea 2.png";
import coffee from "../assets/images/coffee (1).png";
import logo from "../assets/images/logo.png";
import greentea from "../assets/images/green-tea.png";
import Profile from "../components/Dashboard/Profile";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import axios from "axios";
import firebase from "../app/firebaseConfig";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
import gif from "../assets/images/giphyunscreen.gif";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import HelpIcon from "@mui/icons-material/Help";
import { Grid } from "@mui/material";
import quotes from "../Quotes.json";
import evening from '../Quotes.json'
import ClickAwayListener from '@mui/material/ClickAwayListener';
const randomQuotes = quotes;
const eveningRandomQuotes=evening;
const useStyle = makeStyles(() => ({
  CardBox: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    height: [600, 200],
    borderRadius: "10px",
    marginTop: "20px",
    alignItems: "center",
  },
  heading: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  profile: {
    display: "flex !important",
    // flexDirection: "row",
    justifyContent: "space-evenly",
    //  paddingRight:'20px',
    width: "200px",
    alignItems: "center",
  },
  button: {
    width: "20px",
    height: "20px",
  },
  logo: {
    width: "200px",
    height: "100px",
  },
  Slider: {
    width: "100%",
    height: "500px",
    marginTop: "0px !important",
  },
  selectContainer: {
    marginTop: "50px",
    textAlign: "center,",
  },
  modalBox: {
    width: "300px",
  },
  zoom: {
    padding: 50,
    margin: "0 0",
    transition: "transform .2s",
    "&:hover": {
      transform: "scale(1.5)",
    },
  },
  quotes:{
	  color:'green',
	  fontFamily:'fantasy',
	  fontWeight:'bold',
  },

  tooltipCss:{
	  padding:"12px 5px !important",
	  fontSize:"12px",
	  fontWeight:"bold",
  }
}));

const Dashboard = () => {
  let details = (
    <b>
      1). Hi Welcome to zestcafe ! <br />
      <br />
      2). You can select only one cup to have <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; for morning and evening.
      <br />
      <br />
      3). This site will be available from morning <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9 to 11 and evening 3
      to 5. <br />
      <br />
      4). Please select your item before time <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; run's out.
      <br />
      <br />
      5). Hope you will enjoy a good cup !
    </b>
  );
  const date = new Date();
  const hours = date.getHours();
  const [user, setUser] = useState();
  const [selection, setSelection] = useState(false);
  const [buttonType, setButtonType] = useState("");
  const [userChoice, setUserChoice] = useState("");
  const auth = getAuth();
  const navigate = useNavigate();
  const classes = useStyle();

  const user_profile_data = JSON.parse(
    localStorage.getItem("google_user_profile")
  );
  const currentUser = auth?.currentUser || user_profile_data;

  

  const [quotes, setQuotes] = useState([]);
  useEffect(() => {
    const randomLines = randomQuotes;
    let RandomQuotes = Math.floor(Math.random() * randomLines.qoutos.length);
    setQuotes(randomLines.qoutos[RandomQuotes]);
  }, []);
  const [eveningQuotes, setEveningQuotes] = useState([]);
  console.log(eveningQuotes,"eveningQuoteseveningQuotes")
  useEffect(() => {
    const EveningRandomLines = eveningRandomQuotes;
	console.log(EveningRandomLines,"EveningRandomLinesEveningRandomLines")
    let RandomQuotes = Math.floor(Math.random() * EveningRandomLines.evening.length);
    setEveningQuotes(EveningRandomLines.evening[RandomQuotes]);
  }, []);

  React.useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        const uid = user.uid;
      } else {
        navigate("/");
      }
    });
  }, [auth]);

  const baseURL =
    "https://zestcafe-4edf3-default-rtdb.asia-southeast1.firebasedatabase.app/zestgeekDatabase.json";
  const zestgeekUser = firebase.database().ref("zestgeekDatabase");
  const givenName = currentUser?.displayName;
  const imageUrl = currentUser?.photoURL;
  const googleId = currentUser?.uid;

  const getUserDetails = () => {
    axios.get(baseURL).then((response) => {
      const responseData = response.data;
      var userList = [];
      for (var key in responseData) {
        userList.push(responseData[key]);
      }
      setUser(userList);
    });
  };

  useEffect(() => {
    getUserDetails();
  }, []);

  
  // const [timer, setTimer] = useState(true);
  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(false);

  // useEffect(() => {
  //   if (hours >= 8 && hours <= 12) {
  //     setTimer(true);
  //   } else {
  //     setTimer(true ? hours >= 12 && hours <= 20 : "false");
  //   }
  // }, [hours]);

  const handleClick = (e) => {
    e.preventDefault();
    const buttonType = e.target.name || e.target.buttonName;
    setButtonType(buttonType);
    const filterUser = user.find((item) => item.givenName === givenName);
    console.log(buttonType,filterUser,"kjhjhjhj")
    const ButtonFilterName = user.find((item) => item.name === buttonType);
    if (!filterUser) {
      setSelection(true);
      if ((hours >= 8 && hours <= 15) || (hours >= 15 && hours <= 21)) {
        // in working hours
        Swal.fire({
			icon: "success",
			title: `We have received your request`,
			showConfirmButton: false,
			timer: 1500,
		  }).then(() => {
				updateDB(buttonType, givenName, imageUrl, googleId);
        })
      } else {
        Swal.fire({
          title: "Oops! Zestcafe will available 2:50pm to 4:00pm",
          imageWidth: 400,
          imageHeight: 200,
        });
      }
    } else {
      const user_choice = filterUser.name;
      setUserChoice(user_choice);
      if (buttonType === user_choice) {
         setSelection(true);
      } else {
        if ((hours >= 8 && hours <= 15) || (hours >= 15 && hours <= 21)) {
          updateDB(buttonType, givenName, imageUrl, googleId);
		  Swal.fire({
			icon: "success",
			title: `We have change your request`,
			showConfirmButton: false,
			timer: 2000,
		  }).then(() => {
          })
        } 
      else {
			Swal.fire({
				icon: "error",
				title: "Oops! Zestcafe will available tomorrow 9:00am to 11:00am",
				showConfirmButton: false,
				timer:2000
			  });
        }
      }
    }
  };
  const updateDB = (type, givenName, imageUrl, googleId) => {
    zestgeekUser
      .child(googleId)
      .update({
        googleId: googleId,
        name: type,
        givenName: givenName,
        imageUrl: imageUrl,
      })
      .then(() => {
        getUserDetails();
      });
  };

  const [tooltip,setTooltip]=useState(false)
  const handleTooltipClose = () => {
    setTooltip(false);
  };
  const handleTooltipOpen = () => {
    setTooltip(!tooltip);
  };

  return (
    <Box
      className="body"
      sx={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "auto",
        // minHeight: 900,
        alignItem: "center",
        backgroundColor: "#fff",
      }}
    >
      <Box className={classes.heading}>
        <img className={classes.logo} src={logo} alt="logo"></img>
        <Box className={classes.profile}>
			<ClickAwayListener onClickAway={handleTooltipClose} >
		<div >
          <Tooltip   	  
		  PopperProps={{ disablePortal: true,}} 
		  title={
            <div className={classes.tooltipCss} >{details}</div>
          } 
		  disableHoverListener
		   disableFocusListener
		  disableTouchListener
		  onClose={handleTooltipClose}
		  open={tooltip}
		  >
            <IconButton size={"large"} color={"success"} onClick={handleTooltipOpen}  >
              <HelpIcon fontSize={"inherit"} />
            </IconButton>
          </Tooltip>
		  </div>
		  </ClickAwayListener>
          <Profile imageUrl={imageUrl} givenName={givenName} />
        </Box>
      </Box>
      <Box className={classes.selectContainer}>
        <Box sx={{ textAlign: "center" }}>
          <Typography variant="h5" gutterBottom component="div">
            Welcome{" "}
            {currentUser?.displayName ? currentUser?.displayName : "null"} we
            are happy to serve you a better cup.
          </Typography>
          <Typography  variant="h6" gutterBottom component="div">
			  { hours>=8 && hours<=11 ?  <p className={classes.quotes}>"{quotes}"</p>  : <p className={classes.quotes}>"{eveningQuotes}"</p>}          
          </Typography>
          <Typography variant="h5" gutterBottom component="div">
            {/* {timer ? (
              <></>
            ) : (
              Swal.fire({
                title: "Will back soon...",
                width: 600,
                padding: "2em",
                marginTop: "5em",
                color: "#6a9cdd",
                background:
                  ' #6a9cdd url("https://sweetalert2.github.io/images/trees.png")',
                backdrop: ` #789ee666 url(${gif}) right top no-repeat`}).then(()=>{navigate('/')})
                )} */}
          </Typography>
        </Box>
        <Box
          className={classes.CardBox}
          sx={{
            flexDirection: ["column", "row"],
            justifyContent: ["end", null, null],
            alignItem: ["start", null],
            marginTop: ["200px", null, null],
          }}
        >
          <CustomButton
            image={tea}
            buttonName="Tea"
            onClick={true ? handleClick : false}
            user={user}
            givenName={givenName}
            //  disabled={ true ? selection : false }
            selection={selection ? buttonType === "Tea" : false}
          />
          <CustomButton
            image={coffee}
            buttonName="Coffee"
            onClick={true ? handleClick : false}
            user={user}
            givenName={givenName}
            selection={selection ? buttonType === "Coffee" : false}
            //  disabled={ true ? selection : false }
          />
          <CustomButton
            image={greentea}
            buttonName="greentea"
            onClick={true ? handleClick : false}
            user={user}
            givenName={givenName}
            selection={selection ? buttonType === "greentea" : false}
            //  disabled={ true ? selection : false }
          />
        </Box>
      </Box>
      <Box sx={{ paddingTop: "20px", height: "150px" }}></Box>
    </Box>
  );
};

export default Dashboard;
