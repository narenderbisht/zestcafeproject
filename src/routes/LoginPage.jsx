import React, { useEffect } from "react";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";

import { Grid, Typography, Card, Button } from "@mui/material";
import { makeStyles } from "@mui/styles";

import {
  getAuth,
  signInWithRedirect,
  GoogleAuthProvider,
  signInWithPopup,
  onAuthStateChanged,
} from "firebase/auth";

import firebaseConfig from "../app/firebaseConfig";
import { useDispatch } from "react-redux";

const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #fe5713 15%, #50E3C2 90%)",
    minWidth: "100%",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  card: {
    maxWidth: "80%",
    minHeight: "20vh",
    display: "flex",
    alignItems: "center",
    backgroundColor: "transparent !important",
    boxShadow: "none !important",
  },
});

export default function LoginPage() {
  const navigate = useNavigate();
  const classes = useStyles();
  const dispatch = useDispatch();
  const auth = getAuth();

  useEffect(() => {
  onAuthStateChanged(auth, (user) => {
        if (user) { 
            console.log("Firebase auth user :: ", user)
            const uid = user.uid;
            console.log("User Logged in...... navigating to dashboard",uid)
            navigate('/dashboard')
        }
    }
	);
}, [auth, navigate]);

  const handleGoogleSignIn = () => {
    const provider = new GoogleAuthProvider();
	console.log(provider,"providerprovider")
    const auth = getAuth();
	console.log(auth,"authauth")
    signInWithPopup(auth, provider).then((res) => console.log(res, "resss"))
  };

  return (
    <>
      <Grid
      container
        className={classes.root}
        spacing={0}
        alignItems="center"
        justify="center"
      >
        <Card className={classes.card}>
          <Box sx={{ width: "100%", margin: "auto", align: "center" }}>
            <Typography
              variant="h2"
              component="div"
              gutterBottom
              sx={{
                fontWeight: "600",
                color: "#fff",
                background: "transparent !important",
              }}
            >
              Welcome to ZestCafe
            </Typography>
            <div align="center">
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                onClick={handleGoogleSignIn}
              >
                <img
                  width="20px"
                  style={{ marginBottom: "3px", marginRight: "5px" }}
                  alt="Google sign-in"
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"
                />
                Login with Google
              </Button>
            </div>
          </Box>
        </Card>
      </Grid>
    </>
  );
}
